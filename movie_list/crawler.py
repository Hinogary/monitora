import asyncio
import aiohttp
from aiohttp_retry import RetryClient, ExponentialRetry
from asgiref.sync import sync_to_async

from .models import Actor, Movie
from .parser import find_csfd_movie_links, parse_csfd_movie


@sync_to_async
def insert_to_db(movie, actors):
    print(f"saving to db: {movie.name}")
    Actor.objects.bulk_create(actors, ignore_conflicts=True)
    movie.save()
    movie.actors.set(actors)


class Crawler:
    def __init__(self, limit_connections: int, pages: int):
        options = ExponentialRetry(attempts=3, statuses=[429])
        self.client = RetryClient(retry_options=options)
        # only limit number of concurent connections is really simple rate-limiter, but it works in this case
        self.limit_connections = asyncio.Semaphore(limit_connections)
        self.pages = pages

    async def launch(self):
        await asyncio.gather(
            *(self.get_top_movies(page) for page in range(self.pages))
        )
        await self.client.close()

    async def get_page(self, address) -> (aiohttp.ClientResponse, str):
        async with self.limit_connections:
            print(f"get: {address}")
            response = await self.client.get(address, raise_for_status=True)
            content = await response.text()
            return  (response, content)

    async def get_top_movies(self, page: int):
        address = f"https://www.csfd.cz/zebricky/filmy/nejlepsi/?from={100*page}"

        response, content = await self.get_page(address)

        links = find_csfd_movie_links(content)

        movie_responses = await asyncio.gather(
            *(self.get_page(link) for link in links)
        )

        for movie_response in movie_responses:
            movie, actors = parse_csfd_movie(*movie_response)
            await insert_to_db(movie, actors)
