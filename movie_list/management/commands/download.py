import asyncio
from django.core.management.base import BaseCommand, CommandError

from ...crawler import Crawler

class Command(BaseCommand):
    help = 'Crawle database from CSFD'

    def add_arguments(self, parser):
        parser.add_argument('-top_pages', type=int, default=10, help='Number of Top pages download (every page is 100 movies)')
        parser.add_argument('-concurrent_connections', type=int, default=50, help='Number of maximum concurrent connections at one time')

    def handle(self, *args, **options):
        limit_connections = options["concurrent_connections"]
        pages = options['top_pages']
        crawler = Crawler(limit_connections, pages)

        loop = asyncio.get_event_loop()
        loop.run_until_complete(crawler.launch())
