from django.db import models


class Actor(models.Model):
    csfd_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    name_ascii = models.CharField(max_length=100)


class Movie(models.Model):
    csfd_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    name_ascii = models.CharField(max_length=100)
    actors = models.ManyToManyField(Actor, related_name="movies")
