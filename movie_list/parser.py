import re
from bs4 import BeautifulSoup
from unidecode import unidecode

from .models import Movie, Actor

def find_csfd_movie_links(content) -> list[str]:
    soup = BeautifulSoup(content, features="html5lib")
    links = []
    for element in soup.select('.film-title-name'):
        links.append(f"https://www.csfd.cz{element['href']}")
    return links


def parse_csfd_movie(response, content) -> (Movie, list[Actor]):
    soup = BeautifulSoup(content, features="html5lib")
    movie_name = soup.select('h1')[0].getText().strip()
    movie = Movie(
        csfd_id=int(re.search('\d+', response.url.path).group(0)),
        name=movie_name,
        name_ascii=unidecode(movie_name).lower()
    )
    actors_section = None
    for h4 in soup.select('h4'):
        if h4.getText().strip() == 'Hrají:':
            actors_section = h4.parent
            break
    actors = []
    if actors_section is None:
        return movie, actors
    for actor in actors_section.select('a'):
        # link více/méně
        if actor['href'] == '#':
            continue
        actors.append(
            Actor(
                csfd_id=int(re.search('\d+', actor['href']).group(0)),
                name=actor.getText(),
                name_ascii=unidecode(actor.getText()).lower()
            )
        )
    return movie, actors
