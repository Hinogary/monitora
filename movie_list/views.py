from unidecode import unidecode
from django.shortcuts import render
from django.http import Http404

from .models import Movie, Actor


def index(request):
    search =  unidecode(request.GET.get('search', '')).lower()

    movies = Movie.objects.filter(name_ascii__contains=search).all()
    actors = Actor.objects.filter(name_ascii__contains=search).all()

    return render(
        request,
        'list.html',
        {
            'search': search,
            'title': 'List filmů a herců',
            'movies': movies,
            'actors': actors,
        }
    )


def actor(request, actor_id: int):
    try:
        actor = Actor.objects.get(csfd_id=actor_id)
    except Actor.DoesNotExist:
        raise Http404

    return render(
        request,
        'actor.html',
        {
            'title': f'Herec - {actor.name}',
            'actor': actor,
            'movies': actor.movies.all(),
        }
    )


def movie(request, movie_id: int):
    try:
        movie = Movie.objects.get(csfd_id=movie_id)
    except Movie.DoesNotExist:
        raise Http404

    return render(
        request,
        'movie.html',
        {
            'title': f'Film - {movie.name}',
            'movie': movie,
            'actors': movie.actors.all(),
        }
    )
