Launch commands
---------------

Tested on Python 3.10

1. install python modules in `requirements.txt`
2. Init database with `python manage.py migrate`
3. Download films with `python manage.py download` (if you want just top 300 use argument `-top_pages 3`, default is 10). Download can fail on too many connections, you can limit connections with argument `-concurrent_connections` (default is 50).
4. Start server with `python manage.py runserver`
